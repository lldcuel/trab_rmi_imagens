package vLocalhost;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe responsável pela inicialização dos servers.
 */
public class CriaServersRMI {
    
    /**
     * 
     * @param args 
     */
    public static void main(String args[]) {
        int i;
        Thread t;
        ImagemServer svr;
        
        for(i=0; i<Constantes.QTDADE_SERVERS; i++){
            try {
                svr = new ImagemServer(i+Constantes.PORTA_BASE);
                t = new Thread(svr);
                t.start();
            } catch (RemoteException ex) {
                Logger.getLogger(CriaServersRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
