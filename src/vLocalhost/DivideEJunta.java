package vLocalhost;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * 
 */
public class DivideEJunta {
    
    /**
     * Método responsável pela divisão da imagem em n partes.
     * @param filename
     * @param partes
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void split(String filename, int partes) throws FileNotFoundException, IOException {
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        BufferedImage image = ImageIO.read(fis);

        int linhas = partes;
        int colunas = 1;
        int linhasXcolunas = linhas * colunas;

        int X = image.getWidth() / colunas;
        int Y = image.getHeight() / linhas;
        int count = 0;

        BufferedImage buffImage[] = new BufferedImage[linhasXcolunas];
        
        for (int x = 0; x < linhas; x++) {
            for (int y = 0; y < colunas; y++) {
                buffImage[count] = new BufferedImage(X, Y, image.getType());

                Graphics2D gr = buffImage[count++].createGraphics();
                gr.drawImage(image, 0, 0, X, Y, X * y, Y * x, X * y + X, Y * x + Y, null);
                gr.dispose();
            }
        }

        for (int i = 0; i < buffImage.length; i++) {
            ImageIO.write(buffImage[i], "jpg", new File("parte" + i + ".jpg"));
        }
    }
    
    /**
    * Método responsável pela junção de n partes da imagem.
    * @param partes
    * @param dir
    * @throws IOException
    */
    public void merge(int partes, String dir) throws IOException {
        int linhas = partes;
        int colunas = 1;
        int linhasXcolunas = linhas * colunas;

        int X, Y;
        int type;

        File[] imageFiles = new File[linhasXcolunas];
        
        for (int i = 0; i < linhasXcolunas; i++) {
            imageFiles[i] = new File("Cparte" + i + ".jpg");
        }

        BufferedImage[] buffImages = new BufferedImage[linhasXcolunas];
        for (int i = 0; i < linhasXcolunas; i++) {
            buffImages[i] = ImageIO.read(imageFiles[i]);
        }
        
        type = buffImages[0].getType();
        X = buffImages[0].getWidth();
        Y = buffImages[0].getHeight();

        BufferedImage imagemConvertida = new BufferedImage(X * colunas, Y * linhas, type);

        int number = 0;
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                imagemConvertida.createGraphics().drawImage(buffImages[number], X * j, Y * i, null);
                number++;
            }
        }
        ImageIO.write(imagemConvertida, "jpeg", new File(dir + "imagemConvertida.jpg"));
    }

}
