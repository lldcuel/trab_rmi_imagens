package vLocalhost;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Executa {
    
    public static void main(String args[]) {
        long tempInicial = System.currentTimeMillis();
        int i;
        Thread t;
        ImagemClient clt;
        DivideEJunta dej = new DivideEJunta();
        SetFinish sf = new SetFinish();
        String filename = args[0];
        String dir = args[1];
        
        try {
            dej.split(filename, Constantes.QTDADE_SERVERS);
        } catch (IOException ex) {
            Logger.getLogger(Executa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(i=0; i<Constantes.QTDADE_SERVERS; i++){
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(Executa.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            clt = new ImagemClient(i+Constantes.PORTA_BASE, "parte" + i + ".jpg", sf);
            t = new Thread(clt);
            t.start();
        }
        
        while(true){
            if(sf.getFinish()){
                break;
            }
        }
        
        try {
            dej.merge(Constantes.QTDADE_SERVERS, dir);
        } catch (IOException ex) {
            Logger.getLogger(Executa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(i=0; i<Constantes.QTDADE_SERVERS; i++){
            File f = new File("parte" + i + ".jpg");
            f.delete();
            File f2 = new File("Cparte" + i + ".jpg");
            f2.delete();
        }
        long tempFinal = System.currentTimeMillis();
        long tempTotal = (tempFinal - tempInicial) - (Constantes.QTDADE_SERVERS * 200);
        System.out.println("o tempo total gasto foi: " + tempTotal + " milissegundos");
    }
    
}
