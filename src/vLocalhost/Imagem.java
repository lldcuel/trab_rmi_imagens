package vLocalhost;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
  *  
  */
public interface Imagem extends Remote {
    
    /**
     * 
     * @param buffer
     * @return
     * @throws RemoteException 
     */
    byte[] converterEmCinza(byte[] buffer) throws RemoteException;
    
}
