package vLocalhost;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * 
 * 
 */

public class ImagemClient implements Runnable {
    
    private static byte[] retorno = null;
    private static Imagem obj = null;
    private final int porta;
    private final int number;
    private final String filename;
    private final SetFinish sf;
    
    /**
     * 
     * @param porta
     * @param filename
     * @param sf 
     */
    public ImagemClient(int porta, String filename, SetFinish sf){
        this.number = porta-Constantes.PORTA_BASE;
        this.sf = sf;
        this.porta = porta;
        this.filename = filename;
    }
    
    /**
     * 
     */
    @Override
    public void run() {
        try {
            Registry registry = LocateRegistry.getRegistry(porta);
            obj = (Imagem) registry.lookup("//localhost:"+porta+"/ConverterEmCinza");
            
            InputStream is;
            byte[] buffer = null;
            try{
                is = new FileInputStream(filename);
                buffer = new byte[is.available()];
                is.read(buffer);
                is.close();
            }catch(Exception e) {
                
            }
            
            retorno = obj.converterEmCinza(buffer);
            System.out.println("1 chamada remota retornou");
            
            BufferedImage image;
            try {
                image = ImageIO.read( new ByteArrayInputStream( retorno ) );
                ImageIO.write(image, "jpg", new File("Cparte"+number+".jpg"));
            } catch (IOException ex) {
                Logger.getLogger(ImagemClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            sf.setFinishTrue(number);
        }
        catch (NotBoundException | RemoteException e) {
            System.out.println("ConverterEmCinzaClient exception: " + e.getMessage());
        }
    }
}
