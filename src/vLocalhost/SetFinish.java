package vLocalhost;

import java.util.Arrays;

/**
 * 
 */
public class SetFinish {
    
    private final boolean[] finish;
    
    /**
     * 
     * @param i 
     */
    public void setFinishTrue(int i){
        this.finish[i] = true;
    }
    
    /**
     * 
     * @return 
     */
    public boolean getFinish(){
        boolean retorno = true;
        for(int z=0; z<finish.length; z++){
            retorno = retorno && this.finish[z];
        }
        return retorno;
    }
    
    /**
     * 
     */
    public SetFinish(){
        this.finish = new boolean[Constantes.QTDADE_SERVERS];
        Arrays.fill(finish, false);
    }

}
