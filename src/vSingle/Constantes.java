package vSingle;

/**
 * 
 */
public class Constantes {
    
    public static int QTDADE_SERVERS = 4;
    public static int PORTA_BASE = 33333;
    public static double RED_CONS = 0.299;
    public static double GREEN_CONS = 0.587;
    public static double BLUE_CONS = 0.114;
    
    /**
     * 
     * @param qt 
     */
    public static void setQtdadeServers(int qt){
        Constantes.QTDADE_SERVERS = qt;
    }

}
