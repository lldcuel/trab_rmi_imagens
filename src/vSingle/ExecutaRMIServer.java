package vSingle;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe responsável pela inicialização dos servers.
 */
public class ExecutaRMIServer {

    /**
     * 
     * @param args 
     */
    public static void main(String args[]) {
        ImagemServer svr;
        int porta = 33333;

        try {
            svr = new ImagemServer(porta);
            svr.Start();
            
        } catch (RemoteException ex) {
            Logger.getLogger(ExecutaRMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
