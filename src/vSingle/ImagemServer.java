package vSingle;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * 
 */
public class ImagemServer extends UnicastRemoteObject implements Imagem {

    private final int port;

    public ImagemServer(int port) throws RemoteException {
        this.port = port;
    }

    /**
     * 
     * @param imagem
     * @return 
     */
    @Override
    public byte[] converterEmCinza(byte[] imagem) {
        System.out.println("Invocation to converterEmCinza was succesful on port " + this.port);
        
        BufferedImage bufferImage = null;
        int width;
        int height;
        byte[] imageInByte = null;
        
        try {
            InputStream in = new ByteArrayInputStream(imagem);
            bufferImage = ImageIO.read(in);
            width = bufferImage.getWidth();
            height = bufferImage.getHeight();

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    Color c = new Color(bufferImage.getRGB(j, i));
                    int red = (int) (c.getRed() * Constantes.RED_CONS);
                    int green = (int) (c.getGreen() * Constantes.GREEN_CONS);
                    int blue = (int) (c.getBlue() * Constantes.BLUE_CONS);
                    Color newColor = new Color(red + green + blue, red + green + blue, red + green + blue);
                    bufferImage.setRGB(j, i, newColor.getRGB());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ImagemServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        try {
            ImageIO.write(bufferImage, "jpg", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        } catch (IOException ex) {
            Logger.getLogger(ImagemServer.class.getName()).log(Level.SEVERE, null, ex);
        }

        return imageInByte;
    }

    /**
     * 
     */
    public void Start() {
        ImagemServer obj;
        try {
            LocateRegistry.createRegistry(this.port);
            Registry registry = LocateRegistry.getRegistry(this.port);

            obj = new ImagemServer(this.port);
            registry.rebind("//localhost:" + this.port + "/ConverterEmCinza", obj);
            System.out.println("Ligado no registro da porta " + this.port);
        } catch (RemoteException ex) {
            System.out.println("ConverterEmCinzaServer exception: " + ex.getMessage());
        }
    }

}
